# PEC 2

En este módulo se introduce el uso de elementos gráficos estáticos y física dentro de Unity con el propósito de desarrollar juegos sencillos, pero con un aspecto y un comportamiento típicos de los videojuegos de acción. Unity incluye diferentes formatos de salida para nuestros proyectos; aquí aprovecharemos para ver cómo llevar a cabo el desarrollo en Android, desde el punto de vista tanto de los mecanismos de entrada como de la generación del ejecutable.
Para ello, este módulo se divide en dos partes, una de carácter más teórico y otra más práctica. El propósito de la parte teórica es simplemente dar a conocer el mínimo imprescindible para empezar a trabajar. Sin embargo, debe tenerse en cuenta que no se trata de una explicación autocontenida, ya que existen muchos pequeños detalles. La única manera de profundizar es mediante la práctica y el estudio de la documentación oficial. Para guiar este proceso está la segunda parte del módulo, que es la realmente importante.
La parte teórica se centra en cuatro temas. El primero es una introducción a los sprites, que son el elemento fundamental para gestionar gráficos, sobre todo en los juegos 2D. También aprenderemos a usar sprites múltiples para optimizar recursos. A continuación, hablaremos sobre la física 2D en Unity, que es una herramienta muy útil para poder controlar la interacción entre los elementos de juego de manera sencilla. Se incluyen ejemplos tanto de las herramientas del editor como en formato script. 


En lo que respecta a la parte práctica, y para trabajar más a fondo los aspectos explicados, desarrollaremos un proyecto 2D sencillo pero completo, basado en un popular juego clásico.

***
### Controles
* Moverser: izquierda / derecha
* Agachate: abajo
* Saltar: Z
* Dash: Mantenga X mientras se mueve / salta
* Fuego: X
* Pausar / reanudar el juego: Entrar / volver

### Los elementos adicionales implementados

* Se ha añadido elementos gráficos (fondo, sprites, animaciones ...).
* Se ha añadido sonidos.
* Se lee los insultos y respuestas de un fichero de texto o JSON utilizando el directorio Resources de Unity.

### Estructura de un proyecto
* La estructura del proyecto es la siguiente:
* • PEC: directorio raíz.
* • Animaciones: subdirectorio que contiene las animaciones del proyecto.
* • Escenas: subdirectorio que contiene las escenas del proyecto.
* • Scripts: subdirectorio que contiene los scripts del proyecto.
* • Sprites: subdirectorio que contiene los sprites del proyecto.
* • Recursos: subdirectorio que contiene los recursos de sonido del proyecto.
* • Sounds: subdirectorio que contiene los sonidos del proyecto.

